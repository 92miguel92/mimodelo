//
//  Mesa+CoreDataProperties.swift
//  MiModelo
//
//  Created by Master Móviles on 14/02/2017.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import Foundation
import CoreData


extension Mesa {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Mesa> {
        return NSFetchRequest<Mesa>(entityName: "Mesa");
    }

    @NSManaged public var numero: Int16
    @NSManaged public var tipo: String?
    @NSManaged public var restaurante: Restaurante?
    @NSManaged public var camareros: NSSet?

}

// MARK: Generated accessors for camareros
extension Mesa {

    @objc(addCamarerosObject:)
    @NSManaged public func addToCamareros(_ value: Camarero)

    @objc(removeCamarerosObject:)
    @NSManaged public func removeFromCamareros(_ value: Camarero)

    @objc(addCamareros:)
    @NSManaged public func addToCamareros(_ values: NSSet)

    @objc(removeCamareros:)
    @NSManaged public func removeFromCamareros(_ values: NSSet)

}
