//
//  Restaurante+CoreDataProperties.swift
//  MiModelo
//
//  Created by Master Móviles on 14/02/2017.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import Foundation
import CoreData


extension Restaurante {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Restaurante> {
        return NSFetchRequest<Restaurante>(entityName: "Restaurante");
    }

    @NSManaged public var nombre: String?
    @NSManaged public var tenedores: Int16
    @NSManaged public var direccion: String?
    @NSManaged public var camareros: NSSet?
    @NSManaged public var mesas: NSSet?

}

// MARK: Generated accessors for camareros
extension Restaurante {

    @objc(addCamarerosObject:)
    @NSManaged public func addToCamareros(_ value: Camarero)

    @objc(removeCamarerosObject:)
    @NSManaged public func removeFromCamareros(_ value: Camarero)

    @objc(addCamareros:)
    @NSManaged public func addToCamareros(_ values: NSSet)

    @objc(removeCamareros:)
    @NSManaged public func removeFromCamareros(_ values: NSSet)

}

// MARK: Generated accessors for mesas
extension Restaurante {

    @objc(addMesasObject:)
    @NSManaged public func addToMesas(_ value: Mesa)

    @objc(removeMesasObject:)
    @NSManaged public func removeFromMesas(_ value: Mesa)

    @objc(addMesas:)
    @NSManaged public func addToMesas(_ values: NSSet)

    @objc(removeMesas:)
    @NSManaged public func removeFromMesas(_ values: NSSet)

}
