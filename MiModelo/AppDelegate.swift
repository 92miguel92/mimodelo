//
//  AppDelegate.swift
//  MiModelo
//
//  Created by Master Móviles on 13/02/2017.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if let miDelegate = UIApplication.shared.delegate as? AppDelegate {
            let miContexto = miDelegate.persistentContainer.viewContext
            //creamos el objeto persistente y decimos que es un Usuario
            let c = NSEntityDescription.insertNewObject(forEntityName: "Camarero", into: miContexto) as! Camarero
            //modificamos sus propiedades
            c.id = 0
            c.nombre = "Pepe"
            
            let c2 = NSEntityDescription.insertNewObject(forEntityName: "Camarero", into: miContexto) as! Camarero
            c2.id = 1
            c2.nombre = "Luis"
            
            let c3 = NSEntityDescription.insertNewObject(forEntityName: "Camarero", into: miContexto) as! Camarero
            c3.id = 2
            c3.nombre = "Maria"
            
            let r = NSEntityDescription.insertNewObject(forEntityName: "Restaurante", into: miContexto) as! Restaurante
            r.nombre = "Andaluz"
            r.direccion = "Calle falsa"
            r.tenedores = 2
            
            r.addToCamareros(c)
            r.addToCamareros(c2)
            
            print("hola:\(r.camareros!)")
            //Guardamos el objeto
            try? miContexto.save()
            
            miContexto.delete(r)
            miContexto.processPendingChanges()
            try? miContexto.save()
            
            let consulta = NSFetchRequest<Camarero>(entityName: "Camarero")
            let sortDescriptors = [NSSortDescriptor(key:"nombre", ascending:true)]
            consulta.sortDescriptors = sortDescriptors
            let fetReq = NSFetchedResultsController<Camarero>(fetchRequest: consulta, managedObjectContext: miContexto, sectionNameKeyPath: nil, cacheName: "miCache")

            try! fetReq.performFetch()
            
            if let resultados = fetReq.fetchedObjects {
                print("Hay \(resultados.count) mensajes")
                for mensaje in resultados {
                    //Vemos que solo aparece María que no está relacionada con el restaurante
                    print (mensaje.nombre!)
                    //miContexto.delete(mensaje)
                }
                //try? miContexto.save()
            }
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "MiModelo")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

