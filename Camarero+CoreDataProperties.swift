//
//  Camarero+CoreDataProperties.swift
//  MiModelo
//
//  Created by Master Móviles on 14/02/2017.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import Foundation
import CoreData


extension Camarero {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Camarero> {
        return NSFetchRequest<Camarero>(entityName: "Camarero");
    }

    @NSManaged public var id: Int16
    @NSManaged public var nombre: String?
    @NSManaged public var restaurante: Restaurante?
    @NSManaged public var mesas: NSSet?

}

// MARK: Generated accessors for mesas
extension Camarero {

    @objc(addMesasObject:)
    @NSManaged public func addToMesas(_ value: Mesa)

    @objc(removeMesasObject:)
    @NSManaged public func removeFromMesas(_ value: Mesa)

    @objc(addMesas:)
    @NSManaged public func addToMesas(_ values: NSSet)

    @objc(removeMesas:)
    @NSManaged public func removeFromMesas(_ values: NSSet)

}
